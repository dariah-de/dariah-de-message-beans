package info.textgrid.middleware.messagebeans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;

/**
 *
 */
public class TestMessageBeanAbs {

  /**
   * 
   */
  @Test
  public void testGetCachingPathFromDariahURI() {

    String uri = "hdl:21.T11991/0000-0013-281B-1";
    String expectedFilename = "21.T11991-0000-0013-281B-1";

    // Test filename.
    String filename = MessageConsumerAbs.getPrescaleFilenameOnly(uri);
    if (!filename.equals(expectedFilename)) {
      System.out.println(filename + " != " + expectedFilename);
    }
  }

  /**
   * 
   */
  @Test
  public void testGetPrescaleProcessingList() {

    String uri = "hdl:21.T11991/0000-0013-281B-1";
    FileSystem fs = FileSystems.getDefault();

    String id = uri.substring(4).replace("/", "-").replace("%2F", "-");

    System.out.println(id);

    List<Path> expectedPathList = new ArrayList<Path>();
    expectedPathList.add(
        fs.getPath(MessageConsumerAbs.PRESCALE_PATH + "jpeg-sf1/21.T11991-0000-0013-281B-1.jpeg"));
    expectedPathList.add(
        fs.getPath(MessageConsumerAbs.PRESCALE_PATH + "jpeg-sf2/21.T11991-0000-0013-281B-1.jpeg"));
    expectedPathList.add(
        fs.getPath(MessageConsumerAbs.PRESCALE_PATH + "jpeg-sf4/21.T11991-0000-0013-281B-1.jpeg"));
    expectedPathList.add(
        fs.getPath(MessageConsumerAbs.PRESCALE_PATH + "jpeg-sf8/21.T11991-0000-0013-281B-1.jpeg"));
    expectedPathList.add(
        fs.getPath(MessageConsumerAbs.PRESCALE_PATH + "jpeg-sf16/21.T11991-0000-0013-281B-1.jpeg"));
    expectedPathList.add(
        fs.getPath(MessageConsumerAbs.PRESCALE_PATH + "tile-sf1/21.T11991-0000-0013-281B-1.tiff"));
    expectedPathList.add(
        fs.getPath(MessageConsumerAbs.PRESCALE_PATH + "tile-sf2/21.T11991-0000-0013-281B-1.tiff"));
    expectedPathList.add(
        fs.getPath(MessageConsumerAbs.PRESCALE_PATH + "tile-sf4/21.T11991-0000-0013-281B-1.tiff"));
    expectedPathList.add(
        fs.getPath(MessageConsumerAbs.PRESCALE_PATH + "tile-sf8/21.T11991-0000-0013-281B-1.tiff"));
    expectedPathList.add(
        fs.getPath(MessageConsumerAbs.PRESCALE_PATH + "tile-sf16/21.T11991-0000-0013-281B-1.tiff"));

    String prescaleFilename = MessageConsumerAbs.getPrescaleFilenameOnly(uri);
    List<Path> pathList = MessageConsumerAbs.getPrescaleProcessingList(fs, prescaleFilename);

    // We need to test, if all list elements are existing in each list, the order of elements is not
    // important.
    if (!expectedPathList.containsAll(pathList)) {
      System.out.println(pathList);
      System.out.println("!=");
      System.out.println(expectedPathList);
    }
  }

  /**
   * Testing online: dhcrud read access.
   * 
   * @throws IOException
   */
  // @Test
  public void testGetFromDHCrudOnline() throws IOException {

    String host = "https://trep.de.dariah.eu/1.0/dhcrud/";
    String path = "hdl:21.T11991/0000-001D-8BF4-1/data";
    File tempFile = File.createTempFile("kakki-", "-fakki");

    Client client = ClientBuilder.newClient().property("thread.safe.client", "true");
    WebTarget target = client.target(host).path(path);

    try (Response response = target.request().get()) {

      int status = response.getStatus();
      String reasonPhrase = response.getStatusInfo().getReasonPhrase();

      System.out.println(status + " " + reasonPhrase);

      FileUtils.copyInputStreamToFile(response.readEntity(InputStream.class), tempFile);

      response.close();

    }

    System.out.println(tempFile.getCanonicalPath() + "  -->  " + tempFile.length());
  }

}
