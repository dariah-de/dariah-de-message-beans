package info.textgrid.middleware.messagebeans;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.imaging.ImageInfo;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.eclipse.microprofile.reactive.messaging.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.textgrid.middleware.common.CachingUtils;
import info.textgrid.middleware.model.CrudMessage;

/**
 *
 */
public abstract class MessageConsumerAbs {

  private static final String CLASS_NAME = "{" + MessageConsumerAbs.class.getName() + "} ";

  protected static final String PRESCALE_PATH = "/var/dhrep/digilib/prescale/";

  protected static final int JPEG_QUALITY = 90;
  protected static final int TILESIZE = 512;
  protected static final int MAX_FRACTION = 16;
  protected static final int JPEG_BACKGROUND = 255; // white - important if source is transparent

  protected final static Logger LOGGER = Logger.getLogger(MessageConsumerAbs.class.toString());
  protected final static String OPERATION_KEY = "operation";
  protected final static String FORMAT_KEY = "format";
  protected final static String PUBLIC_KEY = "public";
  protected final static String RECEIVED = "Received message: ";

  protected final static String TEMP_PATH = PRESCALE_PATH + "temp/";
  protected final static String JPEG_FOLDER_PREFIX = "jpeg-sf";
  protected final static String TILE_FOLDER_PREFIX = "tile-sf";
  protected final static String JPEG_SUFFIX = ".jpeg";
  protected final static String TIFF_SUFFIX = ".tiff";


  /**
   * @param msg
   * @return
   */
  CrudMessage fromMessage(Message msg) {
    return fromString((String) msg.getPayload());
  }

  /**
   * @param jsonString
   * @return
   */
  CrudMessage fromString(String jsonString) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.readValue(jsonString, CrudMessage.class);
    } catch (JsonProcessingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return new CrudMessage();
  }

  /**
   * @param theClassName
   * @param theMessage
   */
  protected void logInfo(String theClassName, CrudMessage theMessage) {
    LOGGER.info("{" + theClassName + "} " + RECEIVED + theMessage);
  }

  /**
   * @param thePathToOriginal
   * @param theOutputFile
   */
  protected static void execVipsJpegsave(String thePathToOriginal, String theOutputFile) {

    Runtime rt = Runtime.getRuntime();
    List<String> cmdList = new ArrayList<String>();
    cmdList.add("/usr/bin/vips");
    cmdList.add("jpegsave");
    cmdList.add(thePathToOriginal);
    cmdList.add(theOutputFile);
    cmdList.add("-Q");
    cmdList.add(String.valueOf(JPEG_QUALITY));
    cmdList.add("--background");
    cmdList.add(String.valueOf(JPEG_BACKGROUND));

    try {
      Process res = rt.exec(cmdList.toArray(new String[0]));
      int exit = res.waitFor();
      if (exit != 0) {
        LOGGER.warning(CLASS_NAME + "Command " + cmdList + " returned " + exit);
      } else {
        LOGGER.fine(CLASS_NAME + "Command " + cmdList + " returned " + exit);
      }
    } catch (IOException | InterruptedException e) {
      LOGGER.log(Level.SEVERE, CLASS_NAME, e);
    }
  }

  /**
   * @param thePathToOriginal
   * @param theOutputFile
   * @param theSize
   */
  protected static void execVipsThumbnail(String thePathToOriginal, String theOutputFile,
      int theSize) {
    execVipsThumbnail(thePathToOriginal, theOutputFile, String.valueOf(theSize));
  }

  /**
   * @param thePathToOriginal
   * @param theOutputFile
   * @param theSize
   */
  protected static void execVipsThumbnail(String thePathToOriginal, String theOutputFile,
      String theSize) {

    Runtime rt = Runtime.getRuntime();
    List<String> cmdList = new ArrayList<String>();
    cmdList.add("/usr/bin/vipsthumbnail");
    cmdList.add(thePathToOriginal);
    cmdList.add("--output");
    cmdList.add(theOutputFile + "[Q=" + JPEG_QUALITY + ",background=" + JPEG_BACKGROUND + "]");
    cmdList.add("--size=" + theSize);

    try {
      Process res = rt.exec(cmdList.toArray(new String[0]));
      int exit = res.waitFor();
      if (exit != 0) {
        LOGGER.warning(CLASS_NAME + "Command " + cmdList + " returned " + exit);
      } else {
        LOGGER.fine(CLASS_NAME + "Command " + cmdList + " returned " + exit);
      }
    } catch (IOException | InterruptedException e) {
      LOGGER.log(Level.SEVERE, CLASS_NAME, e);
    }
  }

  /**
   * @param inputFile
   * @param outputFile
   */
  protected static void execVipsTiling(String inputFile, String outputFile) {

    Runtime rt = Runtime.getRuntime();
    List<String> cmdList = new ArrayList<String>();
    cmdList.add("/usr/bin/vips");
    cmdList.add("tiffsave");
    cmdList.add("--compression");
    cmdList.add("jpeg");
    cmdList.add("-Q");
    cmdList.add(String.valueOf(JPEG_QUALITY));
    cmdList.add("--tile");
    cmdList.add("--tile-width");
    cmdList.add(String.valueOf(TILESIZE));
    cmdList.add("--tile-height");
    cmdList.add(String.valueOf(TILESIZE));
    cmdList.add(inputFile);
    cmdList.add(outputFile);

    try {
      Process res = rt.exec(cmdList.toArray(new String[0]));
      int exit = res.waitFor();
      if (exit != 0) {
        LOGGER.warning(CLASS_NAME + "Command " + cmdList + " returned " + exit);
      } else {
        LOGGER.fine(CLASS_NAME + "Command " + cmdList + " returned " + exit);
      }
    } catch (IOException | InterruptedException e) {
      LOGGER.log(Level.SEVERE, CLASS_NAME, e);
    }
  }

  /**
   * @param thePath
   * @param theHash
   */
  protected static void createCacheDir(String thePath, String theHash) {
    new File(thePath + File.separatorChar + theHash).mkdirs();
  }

  /**
   * <p>
   * Create downscaled jpegs and tiled tiffs for scale factors from 1 to MAX_FRACTION.
   * </p>
   *
   * @param id
   * @param idhash
   * @param origFile
   * @throws IOException
   * @throws ImageReadException
   */
  protected static void fractionalScale(String id, String origFile)
      throws IOException, ImageReadException {

    ImageInfo info = Imaging.getImageInfo(new File(origFile));

    for (int i = 1; i <= MAX_FRACTION; i = i * 2) {

      String scaleJpgFile = PRESCALE_PATH + "jpeg-sf" + i + File.separatorChar + id + JPEG_SUFFIX;
      String tileTiffFile = PRESCALE_PATH + "tile-sf" + i + File.separatorChar + id + TIFF_SUFFIX;

      // jpeg-sf1 is the origFile -> no scaling for i=1
      if (i != 1) {
        // scale
        int fractionHeight = info.getHeight() / i;
        int fractionWidth = info.getWidth() / i;
        execVipsThumbnail(origFile, scaleJpgFile, fractionWidth + "x" + fractionHeight);
      }

      execVipsTiling(scaleJpgFile, tileTiffFile);
    }
  }

  /**
   * @param theFileSystem
   * @param thePrescaleFilename
   * @param theModificationTime
   */
  protected static void touchPrescaleFiles(FileSystem theFileSystem, String thePrescaleFilename,
      FileTime theModificationTime) {

    List<Path> pathList = getPrescaleProcessingList(theFileSystem, thePrescaleFilename);

    for (Path p : pathList) {
      try {
        Files.setLastModifiedTime(p, theModificationTime);
      } catch (IOException e) {
        LOGGER.log(Level.SEVERE, CLASS_NAME + "Error touching " + thePrescaleFilename, e);
      }
    }
  }

  /**
   * @param theFileSystem
   * @param thePrescaleFilename
   */
  protected static void removePrescaleFiles(FileSystem theFileSystem, String thePrescaleFilename) {

    List<Path> pathList = getPrescaleProcessingList(theFileSystem, thePrescaleFilename);

    for (Path p : pathList) {
      try {
        Files.delete(p);
      } catch (IOException e) {
        LOGGER.log(Level.SEVERE, CLASS_NAME + "Error deleting " + thePrescaleFilename, e);
      }
    }
  }

  /**
   * @param theFileSystem
   * @param thePrescaleFilename
   * @return
   */
  protected static List<Path> getPrescaleProcessingList(FileSystem theFileSystem,
      String thePrescaleFilename) {

    List<Path> result = new ArrayList<Path>();

    for (int i = 1; i <= MAX_FRACTION; i = i * 2) {
      result.add(theFileSystem.getPath(PRESCALE_PATH + File.separatorChar + JPEG_FOLDER_PREFIX + i
          + File.separatorChar + thePrescaleFilename + JPEG_SUFFIX));
      result.add(theFileSystem.getPath(PRESCALE_PATH + File.separatorChar + TILE_FOLDER_PREFIX
          + i + File.separatorChar + thePrescaleFilename + TIFF_SUFFIX));
    }

    return result;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param theURI
   * @return
   */
  public static String getPrescaleFilenameOnly(String theURI) {
    String completePath = CachingUtils.getCachingPathFromURI(theURI).trim();
    return completePath.substring(completePath.lastIndexOf("/") + 1);
  }

}
