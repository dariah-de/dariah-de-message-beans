package info.textgrid.middleware.messagebeans;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.List;
import java.util.concurrent.CompletionStage;
import org.apache.commons.imaging.ImageReadException;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import info.textgrid.middleware.common.CrudOperations;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.model.CrudMessage;
import info.textgrid.middleware.services.DHConnect;
import io.smallrye.reactive.messaging.annotations.Blocking;
import jakarta.inject.Inject;

/**
 * 
 */
public class DHPrescaleMessageConsumer extends MessageConsumerAbs {

  private static final String CLASS_NAME =
      "{" + DHPrescaleMessageConsumer.class.getSimpleName() + "} ";

  @Inject
  DHConnect dhconnect;

  /**
   * @param message
   * @return
   */
  @Incoming("dhcrud")
  @Blocking
  CompletionStage<Void> onMessage(Message<CrudMessage> message) {

    CrudMessage msg = fromMessage(message);

    // Select image mimetypes via TextGrid mimetypes from dariah-de-common.
    if (TextGridMimetypes.IMAGE_SET.contains(msg.getFormat())) {

      int operation = msg.getOperation();
      String uri = msg.getObjectId().toASCIIString();
      boolean isPublic = msg.isPublic();

      switch (operation) {
        case CrudOperations.CREATE:
          LOGGER.info(CLASS_NAME + "CREATE: Prescaling " + uri);
          prescale(uri, isPublic);
          break;
        case CrudOperations.UPDATE:
          LOGGER.info(CLASS_NAME + "UPDATE: Rescaling " + uri);
          prescale(uri, isPublic);
          break;
        case CrudOperations.DELETE:
          LOGGER.info(CLASS_NAME + "DELETE: Deleting " + uri);
          removePrescales(uri);
          break;
        default:
          LOGGER.warning(CLASS_NAME + "UNKNOWN OPERATION: " + CrudOperations.getName(operation)
              + " on " + uri);
          break;
      }
    }

    return message.ack();
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * set last-modified date of prescales to now, so digilib recognizes them as up-to-date
   *
   * @param textgridUri
   */
  protected static void touchPrescales(String theURI) {

    String prescaleNameFromURI = getPrescaleFilenameOnly(theURI);
    FileTime modificationTime = FileTime.fromMillis(System.currentTimeMillis());
    FileSystem fs = FileSystems.getDefault();

    touchPrescaleFiles(fs, prescaleNameFromURI, modificationTime);
  }

  /**
   * @param textgridUri
   */
  private static void removePrescales(String theURI) {

    String prescaleNameFromURI = getPrescaleFilenameOnly(theURI);
    FileSystem fs = FileSystems.getDefault();

    removePrescaleFiles(fs, prescaleNameFromURI);
  }

  /**
   * <p>
   * http://www.vips.ecs.soton.ac.uk/index.php?title=FAQ#How_can_I_process_huge_images_without_running_out_of_RAM.3F
   * </p>
   *
   * @param theURI
   * @param isPublic
   */
  private void prescale(String theURI, boolean isPublic) {

    // Create temp folder, if not yet existing.
    File tempFolder = new File(TEMP_PATH);

    if (!tempFolder.exists()) {
      boolean tempFolderCreated = tempFolder.mkdirs();
      if (tempFolderCreated) {
        LOGGER.info(CLASS_NAME +
            "Temp folder not yet existing! Created new folder: " + tempFolder.getAbsolutePath());
      } else {
        LOGGER.fine(CLASS_NAME + "Temp folder already existing: " + tempFolder.getAbsolutePath());
      }
    }

    String prescaleFilename = getPrescaleFilenameOnly(theURI);

    // Get prescale pathes.
    FileSystem fs = FileSystems.getDefault();
    List<Path> pathList = getPrescaleProcessingList(fs, prescaleFilename);

    // Create cache folders here, if not yet existing.
    for (Path p : pathList) {
      File pFolder = p.toFile().getParentFile();

      if (!pFolder.exists()) {
        boolean cacheFolderCreated = pFolder.mkdirs();
        if (cacheFolderCreated) {
          LOGGER.info(CLASS_NAME + "Cache folder not yet existing! Created new folder: "
              + pFolder.getAbsolutePath());
        } else {
          LOGGER.fine(CLASS_NAME + "Cache folder already existing: " + pFolder.getAbsolutePath());
        }
      }
    }

    // Get image path and create file.
    File tempFile = new File(tempFolder, prescaleFilename);

    try {
      // Retrieve image from DH-crud and store image to original path.
      this.dhconnect.getObjectData(theURI, tempFile);

      LOGGER.info(CLASS_NAME + "Data file temporarily stored to " + tempFile.getAbsolutePath()
          + " (" + tempFile.length() + " bytes)");

      // Original size JPEG is in jpeg-sf1 (scale factor 1).
      String origFile =
          PRESCALE_PATH + "jpeg-sf1" + File.separatorChar + prescaleFilename + JPEG_SUFFIX;

      // Create JPEG in original size.
      execVipsJpegsave(tempFile.getAbsolutePath(), origFile);
      // Create all the other tiles.
      fractionalScale(prescaleFilename, origFile);

      LOGGER.info(CLASS_NAME + "Scaling COMPLETE for " + prescaleFilename);

    } catch (IOException e) {
      LOGGER.severe(CLASS_NAME + "Could not store data file from DH-crud to "
          + tempFile.getAbsolutePath() + "!");
    } catch (ImageReadException e) {
      LOGGER.warning(
          CLASS_NAME + "Error identifying image properties: " + tempFile.getAbsolutePath());
    } finally {
      // Remove original data file from temp.
      boolean deleted = tempFile.delete();
      if (!deleted) {
        LOGGER.warning(CLASS_NAME + "Could not delete orig data file from temp: "
            + tempFile.getAbsolutePath() + "!");
      } else {
        LOGGER.info(CLASS_NAME + "Temp file deletion complete: " + tempFile.getAbsolutePath());
      }
    }
  }

}
