package info.textgrid.middleware.messagebeans;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.concurrent.CompletionStage;
import org.apache.commons.imaging.ImageReadException;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import info.textgrid.middleware.common.CrudOperations;
import info.textgrid.middleware.common.JPairtree;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.model.CrudMessage;
import io.smallrye.reactive.messaging.annotations.Blocking;

/**
 * 
 */
public class TGPrescaleMessageConsumer extends TGMessageConsumerAbs {

  private static final String CLASS_NAME =
      "{" + TGPrescaleMessageConsumer.class.getSimpleName() + "} ";
  private static final String PUBLIC_STORAGE_PATH = "/data/public/productive/";
  private static final String NONPUBLIC_STORAGE_PATH = "/data/nonpublic/productive/";

  /**
   * @param message
   * @return
   */
  @Incoming("tgcrud")
  @Blocking
  CompletionStage<Void> onMessage(Message<CrudMessage> message) {

    CrudMessage msg = fromMessage(message);

    // Select image mimetypes via TextGrid mimetypes from dariah-de-common.
    if (TextGridMimetypes.IMAGE_SET.contains(msg.getFormat())) {

      int operation = msg.getOperation();
      String uri = msg.getObjectId().toASCIIString();
      boolean isPublic = msg.isPublic();

      switch (operation) {
        case CrudOperations.CREATE:
          LOGGER.info(CLASS_NAME + "CREATE: prescaling " + uri);
          prescale(uri, isPublic);
          break;
        case CrudOperations.UPDATE:
          LOGGER.info(CLASS_NAME + "UPDATE: prescaling " + uri);
          prescale(uri, isPublic);
          break;
        case CrudOperations.DELETE:
          LOGGER.info(CLASS_NAME + "DELETE " + uri);
          removePrescales(uri);
          break;
        case CrudOperations.UPDATEMETADATA:
          LOGGER.fine(CLASS_NAME + "UPDATEMETADATA " + uri);
          touchPrescales(uri);
          break;
        default:
          LOGGER.warning(CLASS_NAME + "UNKNOWN OPERATION: " + CrudOperations.getName(operation)
              + " on " + uri);
          break;
      }
    }

    return message.ack();
  }

  /**
   * set last-modified date of prescales to now, so digilib recognizes them as up-to-date
   *
   * @param textgridUri
   */
  private static void touchPrescales(String textgridUri) {

    String id = textgridUri.substring(9);
    FileTime modificationTime = FileTime.fromMillis(System.currentTimeMillis());
    FileSystem fs = FileSystems.getDefault();

    for (int i = 1; i <= MAX_FRACTION; i = i * 2) {
      Path scaleJpegFile = fs.getPath(PRESCALE_PATH + "jpeg-sf" + i + "/" + id + ".jpeg");
      Path tileTiffFile = fs.getPath(PRESCALE_PATH + "tile-sf" + i + "/" + id + ".tiff");
      try {
        Files.setLastModifiedTime(scaleJpegFile, modificationTime);
        Files.setLastModifiedTime(tileTiffFile, modificationTime);
      } catch (IOException e) {
        LOGGER.severe(CLASS_NAME + "Error touching " + textgridUri);
      }
    }
  }

  /**
   * @param textgridUri
   */
  private static void removePrescales(String textgridUri) {

    String id = textgridUri.substring(9);

    FileSystem fs = FileSystems.getDefault();

    for (int i = 1; i <= MAX_FRACTION; i = i * 2) {
      Path scaleJpegFile = fs.getPath(PRESCALE_PATH + "jpeg-sf" + i + "/" + id + ".jpeg");
      Path tileTiffFile = fs.getPath(PRESCALE_PATH + "tile-sf" + i + "/" + id + ".tiff");
      try {
        Files.delete(scaleJpegFile);
        Files.delete(tileTiffFile);
      } catch (IOException e) {
        LOGGER.severe(CLASS_NAME + "Error deleting " + textgridUri);
      }
    }
  }

  /**
   * <p>
   * http://www.vips.ecs.soton.ac.uk/index.php?title=FAQ#How_can_I_process_huge_images_without_running_out_of_RAM.3F
   * </p>
   *
   * @param textgridUri
   * @param isPublic
   */
  private static void prescale(String textgridUri, boolean isPublic) {

    String storagePath;
    if (isPublic) {
      storagePath = PUBLIC_STORAGE_PATH;
    } else {
      storagePath = NONPUBLIC_STORAGE_PATH;
    }

    String id = textgridUri.substring(9);

    new File(TEMP_PATH).mkdirs();

    String tempLink = TEMP_PATH + id;
    File tempLinkFile = new File(tempLink);

    try {
      JPairtree pairtree;
      pairtree = new JPairtree(textgridUri);
      String sourceFile =
          storagePath + pairtree.getPtreePathString() + pairtree.getPtreeEncodedId();
      LOGGER.info(CLASS_NAME + "Image source: " + sourceFile);

      // original size jpeg is in jpeg-sf1 (scalefactor 1)
      String origFile = PRESCALE_PATH + "jpeg-sf1" + "/" + id + ".jpeg";

      // vipsthumbnail does not like pairtree, but symlink in orig may be useful ;-)
      // remove symlink, if already existing (may be pointing to nonpublic after publish)
      if (tempLinkFile.exists()) {
        tempLinkFile.delete();
      }
      Files.createSymbolicLink(tempLinkFile.toPath(), new File(sourceFile).toPath());
      // create jpeg in original size
      execVipsJpegsave(tempLink, origFile);
      fractionalScale(id, origFile);

    } catch (IOException e) {
      LOGGER.severe(CLASS_NAME + "Error prescaling image: " + textgridUri);
    } catch (ImageReadException e) {
      LOGGER.warning(CLASS_NAME + "Error identifying image properties: " + textgridUri);
    } finally {
      // Remove link to original data file from temp.
      boolean deleted = tempLinkFile.delete();
      if (!deleted) {
        LOGGER.warning(CLASS_NAME + "Could not delete link to orig data file from temp: "
            + tempLinkFile.getAbsolutePath() + "!");
      } else {
        LOGGER.info(
            CLASS_NAME + "Temp link file deletion complete: " + tempLinkFile.getAbsolutePath());
      }
    }
  }

}
