package info.textgrid.middleware.messagebeans;

/**
 *
 */
public abstract class TGMessageConsumerAbs extends MessageConsumerAbs {

  /**
   * @param theURI
   * @return
   */
  protected String getPrescaleHash(String theURI) {
    // Take the first three chars.
    return getPrescaleID(theURI).substring(0, 2);
  }

  /**
   * @param theURI
   * @return
   */
  protected String getPrescaleID(String theURI) {
    // Cut "textgrid:".
    return theURI.substring(9);
  }

}
