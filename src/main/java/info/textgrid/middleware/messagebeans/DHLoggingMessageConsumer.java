package info.textgrid.middleware.messagebeans;

import java.util.concurrent.CompletionStage;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import info.textgrid.middleware.model.CrudMessage;
import io.smallrye.reactive.messaging.annotations.Blocking;

/**
 *
 */
public class DHLoggingMessageConsumer extends MessageConsumerAbs {

  /**
   * @param message
   * @return
   */
  @Incoming("dhcrud")
  @Blocking
  CompletionStage<Void> onMessage(Message<CrudMessage> message) {

    CrudMessage msg = fromMessage(message);
    logInfo(this.getClass().getSimpleName(), msg);

    return message.ack();
  }

}
