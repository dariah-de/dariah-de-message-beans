package info.textgrid.middleware.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;

/**
 *
 */
@ApplicationScoped
public class DHConnect {

  private static final Logger LOGGER = Logger.getLogger(DHConnect.class.getName());
  private static final String CLASS_NAME = "{" + DHConnect.class.getSimpleName() + "} ";

  private String propertiesLocation;
  private Properties properties;
  private URL crudUrl;
  private Client dhclient;

  /**
   * 
   */
  public DHConnect() {

    this.propertiesLocation = "/etc/dhrep/messagebeans/dhmb.properties";

    this.properties = new Properties();
    BufferedInputStream stream;

    try {
      stream = new BufferedInputStream(new FileInputStream(this.propertiesLocation));
      this.properties.load(stream);
      stream.close();

      this.crudUrl = new URL(this.properties.getProperty("crudUrl"));

    } catch (FileNotFoundException e) {
      LOGGER.severe(CLASS_NAME + "Properties file not found!");
    } catch (IOException e) {
      LOGGER.severe(CLASS_NAME + "Unable to get CRUD URL from properties file!");
    }

    if (this.dhclient == null) {
      this.dhclient = ClientBuilder.newClient().property("thread.safe.client", "true");
    }

    LOGGER.info(CLASS_NAME + "dhclient: " + this.dhclient.getConfiguration().getProperties());
  }

  /**
   * @param theUri
   * @param theTempFile
   * @throws IOException
   */
  public void getObjectData(String theUri, File theTempFile) {

    WebTarget target = this.dhclient.target(this.crudUrl.toString()).path(theUri + "/data");

    LOGGER.info(CLASS_NAME + "Reading DH-crud data file from: " + target.getUri());

    try (Response response = target.request().get()) {
      FileUtils.copyInputStreamToFile(response.readEntity(InputStream.class), theTempFile);

      LOGGER.info(CLASS_NAME + "Copied data to temp file: " + theTempFile.getAbsolutePath());

      response.close();

    } catch (IOException e) {

      LOGGER.severe(CLASS_NAME + "ERROR: " + e.getMessage());
    }
  }

}
