package info.textgrid.middleware.model;

import java.net.URI;
import io.quarkus.runtime.annotations.RegisterForReflection;

/**
 *
 */
@RegisterForReflection
public class CrudMessage {

  // TODO Put into common for common crud and message-beans usage?

  public URI objectId; // tguri for tg, pid for dh
  public int operation; // create, update, delete
  public String format; // mimetype
  public String rootId; // collectionId for dh, projectId for tg
  public boolean isPublic; // public or not public

  /**
   * Default constructor required for Jackson serializer
   */
  public CrudMessage() {}

  /**
   *
   */
  @Override
  public String toString() {
    return "CrudMessage{objectId='%s', operation='%d', format='%s', rootId='%s', public='%b'}"
        .formatted(this.objectId, this.operation, this.format, this.rootId, this.isPublic);
  }

  /**
   * @return
   */
  public URI getObjectId() {
    return this.objectId;
  }

  /**
   * @param objectId
   * @return
   */
  public CrudMessage setObjectId(URI objectId) {
    this.objectId = objectId;
    return this;
  }

  /**
   * @return
   */
  public int getOperation() {
    return this.operation;
  }

  /**
   * @param operation
   * @return
   */
  public CrudMessage setOperation(int operation) {
    this.operation = operation;
    return this;
  }

  /**
   * @return
   */
  public String getFormat() {
    return this.format;
  }

  /**
   * @param format
   * @return
   */
  public CrudMessage setFormat(String format) {
    this.format = format;
    return this;
  }

  /**
   * @return
   */
  public String getRootId() {
    return this.rootId;
  }

  /**
   * @param rootId
   * @return
   */
  public CrudMessage setRootId(String rootId) {
    this.rootId = rootId;
    return this;
  }

  /**
   * @return
   */
  public boolean isPublic() {
    return this.isPublic;
  }

  /**
   * @param isPublic
   * @return
   */
  public CrudMessage setPublic(boolean isPublic) {
    this.isPublic = isPublic;
    return this;
  }

}
