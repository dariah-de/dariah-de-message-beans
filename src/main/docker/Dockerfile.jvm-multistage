## Stage 1 : build with maven builder image
FROM maven:3.8.7-eclipse-temurin-17 AS build
COPY . /code/
WORKDIR /code
RUN --mount=type=cache,target=/root/.m2 mvn clean package

## Stage 2 : create the docker final image
#FROM registry.access.redhat.com/ubi8/openjdk-17:1.18
FROM debian:bookworm-slim
ENV LANGUAGE='en_US:en'
WORKDIR /work/
RUN chmod 775 /work
RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install --no-install-recommends -y \
    openjdk-17-jdk \
    libvips-tools \
    # save some space
    && rm -rf /var/lib/apt/lists/*
# We make four distinct layers so if there are application changes the library layers can be re-used
COPY --from=build /code/target/quarkus-app/lib/ /deployments/lib/
COPY --from=build /code/target/quarkus-app/*.jar /deployments/
COPY --from=build /code/target/quarkus-app/app/ /deployments/app/
COPY --from=build /code/target/quarkus-app/quarkus/ /deployments/quarkus/
EXPOSE 8080
ENV JAVA_OPTS="-Dquarkus.http.host=0.0.0.0 -Djava.util.logging.manager=org.jboss.logmanager.LogManager"
ENV JAVA_APP_JAR="/deployments/quarkus-run.jar"
ENTRYPOINT ["/usr/lib/jvm/java-17-openjdk-amd64/bin/java", "-jar", "/deployments/quarkus-run.jar", "-Dquarkus.http.host=0.0.0.0", "-Djava.util.logging.manager=org.jboss.logmanager.LogManager"]
