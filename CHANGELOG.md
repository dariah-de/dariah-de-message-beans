# [4.1.0](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/compare/v4.0.0...v4.1.0) (2024-12-11)


### Features

* **prescaler:** set background color for jpeg images to white, used for handling transparency ([10ed6cc](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/10ed6cce0ac760c5c26bd1e9acff0e9c05776657))

# [4.0.0](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/compare/v3.3.0...v4.0.0) (2024-05-23)


### Bug Fixes

* ...and still more corrections ([c734f9f](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/c734f9f5e7a274f9e1de1ba52768a865dbb543be))
* activate jvm-multistage image ([17a5825](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/17a5825fa53885acedf8b367688bb66fbd40305b))
* adapt channels ([595aeed](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/595aeedaf05f640fe0b1146f93e880897e41854a))
* add *all* beans again ([b655162](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/b6551624fd7f9124fa168b68a3bd5580e270fcb0))
* add distibution repository (?) ([923e185](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/923e185a041ddb251d12e643277b7bf12855a4a5))
* add distribution management ([5eda6d3](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/5eda6d30ca8fc4bfcbf658711fb28a808779b5c9))
* add distribution repo (?) ([1c7856d](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/1c7856d70f25b25ea2b1d504d8bb731863454959))
* add jandex plugin ([125ec39](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/125ec3979324f4abe5dab2736e26c888d8e5c4bb))
* add logging to fix HTTP issue ([1017238](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/1017238cda06c599646c20168ba08fed62205e30))
* add metrics and json output ([6914693](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/6914693abe72d04a792787fe23d79891b6f2f7a6))
* add more logging ([5661ad8](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/5661ad8e13ce330e985f101bc8b0d687a1133750))
* add quarkus jandex and config ([34c6925](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/34c6925ffd0604beede337d5aa31c80ac18ce790))
* add sbom again to pom (?) ([b70e31f](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/b70e31f11956540c3b4d0be4ffaf51f9e6a52dbd))
* add sbom to mvn build params again ([c2b5c06](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/c2b5c06dea151554da855b67175a04a9a3f00e9c))
* add vips to jvm dockerfile ([bc372f7](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/bc372f7cdf1c835c6c4c5889260b1fd98af25267))
* change docker image to install vips in container ([0b2fbcc](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/0b2fbccda4378c97b4e87f0bc463e5fa801e40b8))
* change jandex dep name ([02e442a](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/02e442a90eb2a4b74fde3cad0ce21f8f519fb015))
* clean some code and prepare for dh and tg crud message cofiguration ([f853d51](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/f853d510d7401d6830e6408306b556e92aa360c4))
* cleared some logs ([f8dfd51](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/f8dfd519dbab915c4235f13be46f5851a96782dd))
* correct snapshot version ([13222fc](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/13222fc8cf80c4c199b8b9d7a4a1a31235c1a364))
* **da ist noch ein leerzeichen!!!:** da ist noch ein leerzeichen! ([ad89980](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/ad89980dd0e42953c253ce9d4ad37947c4590846))
* devide channels ([8f18884](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/8f18884e9e1d2e26aae5e7942d04b1995f46eefe))
* fix entrypoint ([71fc4a0](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/71fc4a0f9f2a4c4595251fe15bea128a7f971517))
* fix jandex name in config ([76aba08](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/76aba08b9a16e6fb5f5aad2a8da1adf779dcec4a))
* fix some ubbo-comments ([d92718c](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/d92718c06f683e61d0772017c58e8609c2a2ec5d))
* fixing the issue fix :-D ([31fca05](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/31fca05896d6fd449af771c3ea87bd8c8badf336))
* make quarkus tests normal junit tests ([b6f530b](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/b6f530bf52a3196583fca4262e2b0cfe6da21fd0))
* more fixxxxes ([c2a65bd](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/c2a65bdca8ea1af3455acc0793f165cd67c29f39))
* more logging ([7d25394](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/7d25394394e1f68b51fb1fdca63f8b6057514a58))
* re-install dhcrud beans ([635c08f](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/635c08f5869121b913128c42b31ebebba04d849f))
* remove all channels with name 'crud' ([f83bc4e](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/f83bc4eaa12eba37297b0d6fbc224c0b89934d9c))
* remove cxf io utils, using commons.fileutils now ([8c9f047](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/8c9f0476b2ff639e4b5fe48f74a246aa8805af62))
* remove double logging ([a32c5f1](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/a32c5f176531c1209a0f6ea902205351ef4c1459))
* remove jandex due to dhcrud api issues, using client directly now ([ff1ce61](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/ff1ce618aef82919a02c8ab4a77ea03054dde438))
* remove more things ([f3f8bdb](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/f3f8bdb0eae0756e51583ae6fe27bceb0a18017b))
* remove sbom from gitlab ci --> quarkus? ([f07fa2e](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/f07fa2ebdc747e0bc9ed9eaf86171440ae7d611d))
* remove unused (only logging) message beans ([ecc32d5](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/ecc32d5ef057891b816bb95c703f0ef1f4f2f8be))
* remove unwanted line ([5809635](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/5809635e26a13299f592ce373604a4d77ce5437e))
* set dhclient null with init ([c37554e](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/c37554ea904f5cc6056cb16ff69b2517812e7ee0))
* tabs to spaces! go! go! go! ([3133f8c](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/3133f8cc76cc7c3dfa3bff795ace65cc0d6668f3))
* update jvm-multistage image ([fb9c0ea](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/fb9c0eadee4e3dd9a8403283e60b625dcef9b117))
* use commons io now for file utils ([8e2adf2](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/8e2adf2399fd9b93314da3d7e3a2192a721cda0e))


### Features

* arg ([286c37e](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/286c37e07ce345c6b3644527fdf4c3f271b1d3f5))
* using ampq instead of wildfly now! ([2de3320](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/2de33202a1f507835c8aa074ca73c71ea5ec09ae))


### BREAKING CHANGES

* NO WILDFLY anymore...!

# [3.3.0](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/compare/v3.2.0...v3.3.0) (2023-02-28)


### Features

* drop aptly deployment ([8e4d627](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/8e4d627ae7aa740f6a027255543480856581dac3))

# [3.2.0](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/compare/v3.1.1...v3.2.0) (2022-12-12)


### Bug Fixes

* remove old Jenkinsfile ([b5449ed](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/b5449ed82ce584b390a6b8aa419e8dea294469b2))


### Features

* add new gitlab ci workflow ([112a408](https://gitlab.gwdg.de/dariah-de/dariah-de-message-beans/commit/112a408186a626147e4ce27f054943c5f826bcde))
